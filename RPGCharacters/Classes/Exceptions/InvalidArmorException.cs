﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Classes.Exceptions
{
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException()
        {
            Console.WriteLine("Invalid Armor. Cannot equip!"); 
        }
    }
}
