﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Classes.Exceptions
{
    public class InvalidWeaponException : Exception
    {

        public InvalidWeaponException()
        {
            Console.WriteLine("Invalid Weapon. Cannot equip!");
        }
    }
}
