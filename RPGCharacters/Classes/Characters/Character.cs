﻿using RPGCharacters.Classes.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Classes
{
    public abstract class Character
    {

        public string Name { get; set; }
        public int Level { get; set; }
        public double CharacterDPS { get; set; }
        public PrimaryAttributes _PrimaryAttributes { get; set; }
        public PrimaryAttributes BasePrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes { get; set; } 
        public SecondaryAttributes _SecondaryAttributes { get; set; }
        public Dictionary<ItemSlot, Item> _ItemSlot { get; set; }
        public ArmorType[] ArmorTypeArray { get; set; }
        public WeaponType[] WeaponTypeArray { get; set; }



        /// <summary>
        /// Default Constructor for every new Character where it sets all the default properties.
        /// </summary>
        /// <param name="name">Adds a name to the character from specific character class.</param>
        public Character(string name)
        {
            // Set default values to the properties for a new character
            Name = name;
            Level = 1;
            _ItemSlot = new Dictionary<ItemSlot, Item>();
            TotalPrimaryAttributes = new PrimaryAttributes();
            

        }

        /// <summary>
        /// Calculates TotalPrimaryAttributes when character has armor on.
        /// </summary>
        public void CalculateTotalPrimaryAttriutesWithArmor()
        {
            // Loops through to see if the character has any items equipped.
            foreach (var item in _ItemSlot.Values)
            {
                // Checks if the items are of type Armor.
                if (item.GetType() == typeof(Armor))
                {
                    // Sets item to be Armor and then add ArmorAttributes to TotalPrimaryAttributes.
                    var x = (Armor)item;
                    TotalPrimaryAttributes += x.ArmorAttributes;
                }

            }

            TotalPrimaryAttributes += BasePrimaryAttributes;

            //Console.WriteLine($"Total Dexterity: {TotalPrimaryAttributes.Dexterity}, " +
            //    $"Total Strength: {TotalPrimaryAttributes.Strength}, " +
            //    $"Total Intelligence: {TotalPrimaryAttributes.Intelligence}, " +
            //    $"Total Vitality: {TotalPrimaryAttributes.Vitality}");
        }

        /// <summary>
        /// Calculates characters DPS specifically in each character class.
        /// </summary>
        public abstract void CalculateCharacterDPS();

        /// <summary>
        /// Calculates all the SecondaryAttributes. Gets called in each class beacuse of individual PrimaryAttributes.
        /// </summary>
        public void CalculateSecondaryAttributes()
        {
            _SecondaryAttributes = new SecondaryAttributes()
            {
                Health = BasePrimaryAttributes.Vitality * 10,
                ArmorRating = BasePrimaryAttributes.Strength + BasePrimaryAttributes.Dexterity,
                ElementalResistance = BasePrimaryAttributes.Intelligence
            };

        }

        /// <summary>
        /// Prints out all the stats of the Character.
        /// </summary>
        public virtual void ShowStats()
        {
            Console.WriteLine("NAME: " + Name + "\n" + "LEVEL: " + Level.ToString() +
                "\n" + "Strength: " + BasePrimaryAttributes.Strength.ToString() + "\n" + "Dexterity: " +
                BasePrimaryAttributes.Dexterity.ToString() + "\n" + "Intelligence: " + BasePrimaryAttributes.Intelligence.ToString()
                + "\n" + "Vitality: " + BasePrimaryAttributes.Vitality.ToString()
                + "\n" +
                "Health: " + _SecondaryAttributes.Health.ToString() + "\n" + "ArmorRating: " + _SecondaryAttributes.ArmorRating.ToString()
                + "\n" + "ElementalResistance: " + _SecondaryAttributes.ElementalResistance.ToString() + "\n" + "DPS: " + CharacterDPS.ToString()
                );
        }

        /// <summary>
        /// Equips a weapon to the characters weapon slot.
        /// </summary>
        /// <param name="weapon">Weapon that is added to characters weapon slot.</param>
        /// <exception cref="InvalidWeaponException">If weapon is of invalid weapon type or required level isnt reached.</exception>
        public string EquipItem(Weapon weapon)
        {
            // Checks if the WeaponType is right for character and if RequiredLevel is lower och equal to CharacterLevel.
            if (WeaponTypeArray.Contains(weapon._WeaponType) && Level >= weapon.RequiredLevel)
            {
                // Checks if character already have this WeaponType in the WeaponSlot. 
                if (_ItemSlot.ContainsKey(weapon.ItemSlot))
                {
                    // If character has a weapon in the WeaponSlot. Change weapon to the new one.
                    _ItemSlot[weapon.ItemSlot] = weapon;
                }
                else
                {
                    // If character doesn't have a weapon in the WeaponSlot. Add a new Weapon.
                    _ItemSlot.Add(ItemSlot.Weapon, weapon);
                    
                }
                return "New weapon equipped!";
                //Console.WriteLine($"You have equipped {weapon.Name} which is a {weapon._WeaponType} ");
            }
            else
            {
                // Throws a exception if it doesn't meet the criterias above. 
                throw new InvalidWeaponException();
            }

        }



        /// <summary>
        /// Equips a armor to the characters armor slot.
        /// </summary>
        /// <param name="armor">Armor that is added to characters armor slot.</param>
        /// <exception cref="InvalidWeaponException">If armor is of invalid armor type or required level isn't reached.</exception>
        public string EquipItem(Armor armor)
        {
            // Checks if the ArmorType is right for character and if RequiredLevel is lower och equal to CharacterLevel.
            if (ArmorTypeArray.Contains(armor._ArmorType) && Level >= armor.RequiredLevel)
            {
                // Checks if character already have this ArmorType in the ArmorSlot. 
                if (_ItemSlot.ContainsKey(armor.ItemSlot))
                {
                    // If character has a Armor in the in the right ArmorSlot. Change Armor to the new one.
                    _ItemSlot[armor.ItemSlot] = armor;
                }
                else
                {
                    // If character doesn't have Armor in the ArmorSlot. Add a new Armor.
                    _ItemSlot.Add(armor.ItemSlot, armor);
                }

                return "New armor equipped!";
            }
            else
            {
                // Throws a exception if it doesn't meet the criterias above. 
                throw new InvalidArmorException();
            }

        }

        /// <summary>
        /// Levels up character and adds all the new Attributes. 
        /// </summary>
        /// <param name="levels">Adds how many levels the character should level up</param>
        public abstract int LevelUp(int levels);



    }


}
