﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Classes
{
    public class Mage : Character
    {

        /// <summary>
        /// When a new Mage is created, it gets all the default values for the mage class.
        /// </summary>
        public Mage() : base("Mage Peter")
        {
            BasePrimaryAttributes = new PrimaryAttributes()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8,
                Vitality = 5
            };
            CalculateSecondaryAttributes();
            ArmorTypeArray = new ArmorType[] { ArmorType.Cloth };
            WeaponTypeArray = new WeaponType[] { WeaponType.Staff, WeaponType.Wand };

        }

        /// <summary>
        /// Calculate characterDPS specific for Mage class.
        /// </summary>
        public override void CalculateCharacterDPS()
        {
            //double IntelligenceAsDouble = BasePrimaryAttributes.Intelligence;

            // Checks if there is any Weapon i the weapon slot.
            if (_ItemSlot.ContainsKey(ItemSlot.Weapon))
            {
                // If there is a weapon, Calculate the character DPS.
                Weapon weapon = (Weapon)_ItemSlot[ItemSlot.Weapon];

                weapon.DamagePerSecond = weapon.BaseDamage * weapon.AttacksPerSecond;
                CharacterDPS = weapon.DamagePerSecond * (1 + TotalPrimaryAttributes.Intelligence / 100);

            }
            else
            {
                // If there is no weapon, Calculate Character DPS without weapon.
                CharacterDPS = 1 * (1 + BasePrimaryAttributes.Intelligence / 100);
            }
        }

        /// <summary>
        /// Adds levels to the character and updates all attributes.
        /// </summary>
        /// <param name="levels">Adds the number of levels to Character Level.</param>
        public override int LevelUp(int levels)
        {
            // Checks if the levels input is less than 1.
            if (levels < 1)
            {
                throw new ArgumentException("Level can't be > 1. Try again!");
            }
            else
            {
                // Loops as many times as levels is and adds the level up attributes to the character.
                for (int i = 0; i < levels; i++)
                {
                    var levelUpAttributes = new PrimaryAttributes()
                    {
                        Strength = 1,
                        Dexterity = 1,
                        Intelligence = 5,
                        Vitality = 3
                        
                    };

                    BasePrimaryAttributes += levelUpAttributes;
                    TotalPrimaryAttributes = BasePrimaryAttributes;
                    CalculateSecondaryAttributes();
                }
            };
            return Level += levels;
        }

    }
}
