﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Classes
{
    public class Ranger : Character
    {
        /// <summary>
        /// When a new Ranger is created, it gets all the default values for the Ranger class.
        /// </summary>
        public Ranger() : base("Ranger Peter")
        {
            BasePrimaryAttributes = new PrimaryAttributes() { 
                Strength = 1, 
                Dexterity = 8, 
                Intelligence = 1, 
                Vitality = 5 
            };

            CalculateSecondaryAttributes();
            ArmorTypeArray = new ArmorType[] { ArmorType.Leather, ArmorType.Mail };
            WeaponTypeArray = new WeaponType[] { WeaponType.Bow };

        }

        /// <summary>
        /// Calculate characterDPS specific for Ranger class.
        /// </summary>
        public override void CalculateCharacterDPS()
        {

            // Checks if there is any Weapon i the weapon slot.
            if (_ItemSlot.ContainsKey(ItemSlot.Weapon))
            {
                // If there is a weapon, Calculate the character DPS.
                Weapon weapon = (Weapon)_ItemSlot[ItemSlot.Weapon];

                weapon.DamagePerSecond = weapon.BaseDamage * weapon.AttacksPerSecond;
                CharacterDPS = weapon.DamagePerSecond * (1 + TotalPrimaryAttributes.Dexterity / 100);

            }
            else
            {
                // If there is no weapon, Calculate Character DPS without weapon.
                CharacterDPS = 1 * (1 + BasePrimaryAttributes.Dexterity / 100);
            }
        }

        /// <summary>
        /// Adds levels to the character and updates all attributes.
        /// </summary>
        /// <param name="levels">Adds the number of levels to Character Level.</param>
        public override int LevelUp(int levels)
        {
            // Checks if the levels input is less than 1.
            if (levels < 1)
            {
                throw new ArgumentException("Level can't be > 1. Try again!");
            }
            else
            {
                // Loops as many times as levels is and adds the level up attributes to the character.
                for (int i = 0; i < levels; i++)
                {
                    var levelUpAttributes = new PrimaryAttributes() {
                        Strength = 1,
                        Dexterity = 5, 
                        Intelligence = 1, 
                        Vitality = 2
                        
                    };
                    BasePrimaryAttributes += levelUpAttributes;
                    CalculateSecondaryAttributes();
                    TotalPrimaryAttributes = BasePrimaryAttributes;

                }
                
            };
            return Level += levels;

        }

    }
}
