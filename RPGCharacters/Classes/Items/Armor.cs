﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Classes
{
    /// <summary>
    /// Sets what kind of armortypes there is.
    /// </summary>
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }

    public class Armor : Item
    {
        public PrimaryAttributes ArmorAttributes { get; set; }
        public ArmorType _ArmorType { get; set; }


    }
}
