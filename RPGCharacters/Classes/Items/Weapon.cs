﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Classes
{
    /// <summary>
    /// Sets what kind of weapontypes there is.
    /// </summary>
    public enum WeaponType
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }

    public class Weapon : Item
    {
        public double BaseDamage { get; set; }
        public double AttacksPerSecond { get; set; }
        public double DamagePerSecond { get; set; }
        public WeaponType _WeaponType { get; set; }

    }
}
