﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Classes
{
    /// <summary>
    /// Sets what kind of itemslots there is. 
    /// </summary>
    public enum ItemSlot
    {
        Head,
        Body,
        Legs,
        Weapon
    }


    public abstract class Item
    {
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public ItemSlot ItemSlot { get; set; }

    }
}
