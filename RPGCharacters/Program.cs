﻿using RPGCharacters.Classes;
using System;

namespace RPGCharacters
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Hello!");

            //Warrior warriorObj = new Warrior();
            //warriorObj.LevelUp(10);
            //warriorObj.ShowStats();


            //Rouge rougeObj = new Rouge();
            //rougeObj.LevelUp(3);
            //rougeObj.ShowStats();


            //Ranger rangerObj = new Ranger();
            //rangerObj.LevelUp(5);
            //rangerObj.ShowStats();


            Mage mageObj = new Mage();
            //mageObj.LevelUp(1);

            //Weapon weaponObj = new Weapon()
            //{
            //    _WeaponType = WeaponType.Wand,
            //    Name = "Staff of Hearding",
            //    RequiredLevel = 1,
            //    AttacksPerSecond = 1.6,
            //    BaseDamage = 12,
            //    ItemSlot = ItemSlot.Weapon
            //};
            //mageObj.EquipItem(weaponObj);


            Armor armorObj = new Armor()
            {
                _ArmorType = ArmorType.Cloth,
                ItemSlot = ItemSlot.Head,
                Name = "Helmet",
                RequiredLevel = 1,
                ArmorAttributes = new PrimaryAttributes() { 
                    Dexterity = 1, 
                    Intelligence = 1, 
                    Strength = 1, 
                    Vitality = 1
                }
            };

            Armor armorObj2 = new Armor()
            {
                _ArmorType = ArmorType.Cloth,
                ItemSlot = ItemSlot.Body,
                Name = "Chest armor",
                RequiredLevel = 1,
                ArmorAttributes = new PrimaryAttributes() { 
                    Dexterity = 1, 
                    Intelligence = 1, 
                    Strength = 1, 
                    Vitality = 1 
                }
            };

            mageObj.EquipItem(armorObj);
            mageObj.EquipItem(armorObj2);
            mageObj.CalculateTotalPrimaryAttriutesWithArmor();
            mageObj.CalculateCharacterDPS();

            mageObj.ShowStats();

        }

    }
}
