using RPGCharacters.Classes;
using RPGCharacters.Classes.Exceptions;
using System;
using Xunit;

namespace RPGWeaponTests
{
    public class WeaponTests
    {
        /// <summary>
        /// Equip Weapon of higher level and throw InvalidWeaponException.
        /// </summary>
        // Step 1.
        [Fact]
        public void EquipWeapon_EquipHighLevelWeapon_InvalidWeaponException()
        {
            // Arrange
            Warrior warriorObj = new Warrior();
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                RequiredLevel = 2,
                ItemSlot = ItemSlot.Weapon,
                _WeaponType = WeaponType.Axe,
                BaseDamage = 7,
                AttacksPerSecond = 1.1
            };

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warriorObj.EquipItem(testAxe));
        }


        /// <summary>
        /// Equip armor of higher level and throw InvalidArmorException.
        /// </summary>
        // Step 2.
        [Fact]
        public void EquipArmor_EquipHighLevelArmor_InvalidArmorException()
        {
            // Arrange
            Warrior warriorObj = new Warrior();
            Armor testPlateBody = new Armor()
            {
                Name = "Common plate body armor",
                RequiredLevel = 2,
                ItemSlot = ItemSlot.Body,
                _ArmorType = ArmorType.Plate,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1}
            };

            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => warriorObj.EquipItem(testPlateBody));
        }


        /// <summary>
        /// Equip weapon of wrong weapontype and throw InvalidWeaponException.
        /// </summary>
        // Step 3.
        [Fact]
        public void EquipWeapon_EquipWrongTypeWeapon_InvalidWeaponException()
        {
            // Arrange
            Warrior warriorObj = new Warrior();
            Weapon testBow = new Weapon()
            {
                Name = "Common bow",
                RequiredLevel = 1,
                ItemSlot = ItemSlot.Weapon,
                _WeaponType = WeaponType.Bow,
                BaseDamage = 7,
                AttacksPerSecond = 1.1
            };

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warriorObj.EquipItem(testBow));
        }


        /// <summary>
        /// Equip armor of wrong armortype and throw InvalidArmorException.
        /// </summary>
        // Step 4.
        [Fact]
        public void EquipArmor_EquipWrongTypeArmor_InvalidArmorException()
        {
            // Arrange
            Warrior warriorObj = new Warrior();
            Armor testClothBody = new Armor()
            {
                Name = "Common cloth body armor",
                RequiredLevel = 2,
                ItemSlot = ItemSlot.Body,
                _ArmorType = ArmorType.Cloth,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };

            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => warriorObj.EquipItem(testClothBody));
        }


        /// <summary>
        /// Equip a valid weapon and throw a "New weapon equipped!" message.
        /// </summary>
        //// Step 5.
        [Fact]
        public void EquipWeapon_EquipValidTypeWeapon_NewWeaponEquippedMessage()
        {
            // Arrange
            Warrior warriorObj = new Warrior();
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                RequiredLevel = 1,
                ItemSlot = ItemSlot.Weapon,
                _WeaponType = WeaponType.Axe,
                BaseDamage = 7,
                AttacksPerSecond = 1.1
            };
            warriorObj.EquipItem(testAxe);


            // Act
            string actual = warriorObj.EquipItem(testAxe); 
            string expected = "New weapon equipped!";

            // Assert
            Assert.Equal(expected, actual);
        }


        /// <summary>
        /// Equip a valid armor and throw a "New armor equipped!" message.
        /// </summary>
        //// Step 6.
        [Fact]
        public void EquipArmor_EquipValidTypeArmor_NewArmorEquippedMessage()
        {
            // Arrange
            Warrior warriorObj = new Warrior();
            Armor testPlateBody = new Armor()
            {
                Name = "Common plate body armor",
                RequiredLevel = 1,
                ItemSlot = ItemSlot.Body,
                _ArmorType = ArmorType.Plate,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            warriorObj.EquipItem(testPlateBody);


            // Act
            string actual = warriorObj.EquipItem(testPlateBody);
            string expected = "New armor equipped!";

            // Assert
            Assert.Equal(expected, actual);
        }


        /// <summary>
        /// Calculate level 1 character dps.
        /// </summary>
        // Step 7.
        [Fact]
        public void CalculateDPS_CalculateLevel1DPS_ExpectedDPS()
        {
            // Arrange
            Warrior warriorObj = new Warrior();
            warriorObj.CalculateCharacterDPS();

            // Act
            var actual = warriorObj.CharacterDPS;
            var expected = 1 * (1 + (5 / 100));

            // Assert
            Assert.Equal(expected, actual);
        }


        /// <summary>
        /// Calculate character dps with weapon equipped.
        /// </summary>
        // Step 8.
        [Fact]
        public void CalculateDPSWithWeapon_CalculateDPSWithValidWeapon_ExpectedDPS()
        {
            // Arrange
            Warrior warriorObj = new Warrior();
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                RequiredLevel = 1,
                ItemSlot = ItemSlot.Weapon,
                _WeaponType = WeaponType.Axe,
                BaseDamage = 7,
                AttacksPerSecond = 1.1
            };
            warriorObj.EquipItem(testAxe);
            warriorObj.CalculateCharacterDPS();

            // Act
            var actual = warriorObj.CharacterDPS;
            var expected = (7 * 1.1) * (1 + (5 / 100));

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Calculate character dps with weapon and armor equipped.
        /// </summary>
        // Step 9.
        [Fact]
        public void CalculateDPSWithWeaponAndArmor_CalculateDPSWithValidWeaponAndArmor_ExpectedDPS()
        {
            // Arrange
            Warrior warriorObj = new Warrior();
            Weapon testAxe = new Weapon()
            {
                Name = "Common axe",
                RequiredLevel = 1,
                ItemSlot = ItemSlot.Weapon,
                _WeaponType = WeaponType.Axe,
                BaseDamage = 7,
                AttacksPerSecond = 1.1
            };
            Armor testPlateBody = new Armor()
            {
                Name = "Common plate body armor",
                RequiredLevel = 1,
                ItemSlot = ItemSlot.Body,
                _ArmorType = ArmorType.Plate,
                ArmorAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            warriorObj.EquipItem(testPlateBody);
            warriorObj.EquipItem(testAxe);
            warriorObj.CalculateCharacterDPS();

            // Act
            var actual = warriorObj.CharacterDPS;
            var expected = (7 * 1.1) * (1 + ((5 + 1) / 100));

            // Assert
            Assert.Equal(expected, actual);

        }
    }
}
