using System;
using Xunit;
using RPGCharacters.Classes;

namespace RPGCharactersTests
{
    public class CharacterTests
    {
        /// <summary>
        /// Creates a new character. Checks if level is default set to level 1.
        /// </summary>
        // Step 1.
        [Fact]
        public void CreateCharacter_SetCharacterLevelTo1_SetLevel1()
        {
            // Arrange
            Warrior warriorObj = new Warrior();
            
            // Act
            int actual = warriorObj.Level;
            int expected = 1;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Levels up character 1 level and set level to level 2.
        /// </summary>
        // Step 2.
        [Fact]
        public void LevelUp_CharacterGainsALevel_SetCharacterToLevel2()
        {
            // Arrange
            Warrior warriorObj = new Warrior();

            // Act
            int actual = warriorObj.LevelUp(1);
            int expected = warriorObj.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Set level less than 1. Throws a exception. 
        /// </summary>
        /// <param name="i"></param>
        // Step 3.
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void LevelUp_SetsCharacterLevelEqualToInlineData_SetLevelIfInlineDataIs1OrAbove(int i)
        {
            // Arrange
            Warrior warriorObj = new Warrior();

            // Act 
            Assert.Throws<ArgumentException>(() => warriorObj.LevelUp(i));

            // Assert
        }



        // ******************
        // All Warrior tests.
        // ******************
        /// <summary>
        /// Create a warrior and set default level 1 attributes. 
        /// </summary>
        // Step 4.
        [Fact]
        public void CreateWarrior_CreateWarriorAndSetDefaultAttributes_SetsDefaultAttributes()
        {
            // Arrange
            Warrior warriorObj = new Warrior();

            // Act
            PrimaryAttributes actual = warriorObj.BasePrimaryAttributes;

            PrimaryAttributes expected = new PrimaryAttributes() {Strength = 5, Dexterity = 2, Intelligence = 1, Vitality = 10 };
            // Assert

            Assert.Equal(expected, actual);
        }


        /// <summary>
        /// Create a warrior and level up character to level 2. Set new attributes.
        /// </summary>
        // Step 5.
        [Fact]
        public void CreateWarriorAndLevelUp_CreateWarriorLevelUpAndIncreaseAttributes_SetLevelAndIncreaseAttributes()
        {
            // Arrange
            Warrior warriorObj = new Warrior();
            warriorObj.LevelUp(1);

            // Act
            PrimaryAttributes actual = warriorObj.BasePrimaryAttributes;

            PrimaryAttributes expected = new PrimaryAttributes() { Strength = 8, Dexterity = 4, Intelligence = 2, Vitality = 15 };
            // Assert
            Assert.Equal(expected, actual);
        }


        /// <summary>
        /// Create warrior and level up to level 2. Calculate level 2 secondary attributes.
        /// </summary>
        // Step 6.
        [Fact]
        public void CreateWarriorLevelUpOnce_CalculateSecondaryAttributesFromLeveledCharacter_CalculateSecondaryAttributesWhenLeveledUp()
        {
            // Arrange
            Warrior warriorObj = new Warrior();
            warriorObj.LevelUp(1);

            // Act
            SecondaryAttributes actual = warriorObj._SecondaryAttributes;
            SecondaryAttributes expected = new SecondaryAttributes() { Health = 150, ArmorRating = 12, ElementalResistance = 2 };

            // Assert
            Assert.Equal(expected, actual);
        }



        // ******************
        // All Ranger tests.
        // ******************
        /// <summary>
        /// Create a ranger and set default level 1 attributes. 
        /// </summary>
        // Step 4.
        [Fact]
        public void CreateRanger_CreateRangerAndSetDefaultAttributes_SetsDefaultAttributes()
        {
            // Arrange
            Ranger rangerObj = new Ranger();

            // Act
            PrimaryAttributes actual = rangerObj.BasePrimaryAttributes;

            PrimaryAttributes expected = new PrimaryAttributes() { Strength = 1, Dexterity = 8, Intelligence = 1, Vitality = 5 };
            // Assert

            Assert.Equal(expected, actual);
        }


        /// <summary>
        /// Create a ranger and level up character to level 2. Set new attributes.
        /// </summary>
        // Step 5.
        [Fact]
        public void CreateRangerAndLevelUp_CreateRangerLevelUpAndIncreaseAttributes_SetLevelAndIncreaseAttributes()
        {
            // Arrange
            Ranger rangerObj = new Ranger();
            rangerObj.LevelUp(1);

            // Act
            PrimaryAttributes actual = rangerObj.BasePrimaryAttributes;
            PrimaryAttributes expected = new PrimaryAttributes() { Strength = 2, Dexterity = 13, Intelligence = 2, Vitality = 7 };

            // Assert
            Assert.Equal(expected, actual);
        }



        // ******************
        // All Mage tests.
        // ******************
        /// <summary>
        /// Create a mage and set default level 1 attributes. 
        /// </summary>
        // Step 4.
        [Fact]
        public void CreateMage_CreateMageAndSetDefaultAttributes_SetsDefaultAttributes()
        {
            // Arrange
            Mage mageObj = new Mage();

            // Act
            PrimaryAttributes actual = mageObj.BasePrimaryAttributes;

            PrimaryAttributes expected = new PrimaryAttributes() { Strength = 1, Dexterity = 1, Intelligence = 8, Vitality = 5 };
            // Assert

            Assert.Equal(expected, actual);
        }


        /// <summary>
        /// Create a mage and level up character to level 2. Set new attributes.
        /// </summary>
        // Step 5.
        [Fact]
        public void CreateMageAndLevelUp_CreateMageLevelUpAndIncreaseAttributes_SetLevelAndIncreaseAttributes()
        {
            // Arrange
            Mage mageObj = new Mage();
            mageObj.LevelUp(1);

            // Act
            PrimaryAttributes actual = mageObj.BasePrimaryAttributes;
            PrimaryAttributes expected = new PrimaryAttributes() { Strength = 2, Dexterity = 2, Intelligence = 13, Vitality = 8 };

            // Assert
            Assert.Equal(expected, actual);
        }



        // ******************
        // All Rouge tests.
        // ******************
        /// <summary>
        /// Create a rouge and set default level 1 attributes. 
        /// </summary>
        // Step 4.
        [Fact]
        public void CreateRouge_CreateRougeAndSetDefaultAttributes_SetsDefaultAttributes()
        {
            // Arrange
            Rouge rougeObj = new Rouge();

            // Act
            PrimaryAttributes actual = rougeObj.BasePrimaryAttributes;

            PrimaryAttributes expected = new PrimaryAttributes() { Strength = 2, Dexterity = 6, Intelligence = 1, Vitality = 8 };
            // Assert

            Assert.Equal(expected, actual);
        }


        /// <summary>
        /// Create a rouge and level up character to level 2. Set new attributes.
        /// </summary>
        // Step 5.
        [Fact]
        public void CreateRougeAndLevelUp_CreateRougeLevelUpAndIncreaseAttributes_SetLevelAndIncreaseAttributes()
        {
            // Arrange
            Rouge rougeObj = new Rouge();
            rougeObj.LevelUp(1);

            // Act
            PrimaryAttributes actual = rougeObj.BasePrimaryAttributes;
            PrimaryAttributes expected = new PrimaryAttributes() { Strength = 3, Dexterity = 10, Intelligence = 2, Vitality = 11 };

            // Assert
            Assert.Equal(expected, actual);
        }






    }
}
